#!/bin/sh
export JAVA_HOME=/root/jdk1.8.0_211
export CLASSPATH=.:${JAVA_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH
java -version
nohup java -jar -Dcas.server-url-prefix=http://${cashost}:${casport}/cas -Dcas.client-host-url=http://${clientserver}:${clientport}/login -Dcas-logout-url=http://${cashost}:${casport}/cas/logout -Dcas.server-login-url=http://${cashost}:${casport}/cas/login /root/app.jar &
nginx
